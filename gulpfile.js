const gulp = require( 'gulp' );
const sourcemaps = require( 'gulp-sourcemaps' );
const rename = require( 'gulp-rename' );
const watch = require( 'gulp-watch' );
const uglify = require( 'gulp-uglify' );

const babelify = require( 'babelify');
const browserify = require( 'browserify');
const browserSync = require( 'browser-sync');
const source = require( 'vinyl-source-stream');
const buffer = require( 'vinyl-buffer');
const vinylPaths = require( 'vinyl-paths');
const del = require('del');

const htmlSrcFiles = './src/**/*.html';
const jsMain = 'main.js';
const jsSrcFolder = 'src/';
const jsSrcFiles = jsSrcFolder + '**/*.js";';
const jsDist = './dist/';
const jsWatch = 'src/**/*.js';

gulp.task( 'browserSync', ( ) => {
    browserSync( {
        server: {
            baseDir: jsDist
        }
    } );
});

gulp.task( 'clean', ( ) => {
    return gulp.src( jsDist, { read: false } )
    .pipe( vinylPaths( del ) );
});


compile = (  ) => {
    return browserify({
        basedir: '.',
        debug: true,
        entries: [ jsSrcFolder + jsMain ],
        cache: {},
        packageCache: {}
    })
    .transform( 'babelify' )
    .bundle( )
    .pipe( source( jsMain ) )
    .pipe( rename( { extname: '.min.js' } ) )
    .pipe( buffer( ) )
    .pipe(sourcemaps.init( { loadMaps: true } ) )
    .pipe( uglify( ) )
    .pipe( sourcemaps.write( './' ) )
    .pipe( gulp.dest( jsDist ) )
    .pipe( browserSync.reload( { stream: true } ) );
};

gulp.task( 'compile', compile );

copy = ( ) => {
    return gulp.src( htmlSrcFiles )
    .pipe( gulp.dest( jsDist ) )
    .pipe( browserSync.reload( { stream: true } ) )
};

gulp.task( 'copy', copy );

gulp.task( 'watchSrc', ( ) => {
    watch( htmlSrcFiles, copy);
    watch( jsSrcFiles, { ignore: 'gulpfile.js'}, compile );
});

gulp.task( 'default', gulp.series( 'clean', 'copy', 'compile', 'browserSync', 'watchSrc' ) );
